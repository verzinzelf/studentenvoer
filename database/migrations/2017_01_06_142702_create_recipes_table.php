<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('time')->unsigned()->nullable();
            $table->integer('calories')->unsigned()->nullable();
            $table->integer('persons')->unsigned()->nullable();
            $table->string('price')->nullable();
            $table->integer('category_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('type_id')->unsigned()->nullable();
            $table->text('directions');
            $table->softDeletes();
            $table->nullableTimestamps();

            $table->foreign('category_id', 'fk_recipes_categories_idx')
                ->references('id')->on('categories')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('user_id', 'fk_recipes_users_idx')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('type_id', 'fk_recipes_types_idx')
                ->references('id')->on('types')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
}
