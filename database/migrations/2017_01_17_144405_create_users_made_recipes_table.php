<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersMadeRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_made_recipes', function (Blueprint $table) {
            $table->integer('users_id')->unsigned();
            $table->integer('recipe_id')->unsigned();
            $table->nullableTimestamps();

            $table->primary(['users_id', 'recipe_id']);

            $table->foreign('users_id', 'fk_users_made_recipes_users_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('recipe_id', 'fk_users_made_recipes_recipes_idx')
                ->references('id')->on('recipes')
                ->onDelete('cascade')
                ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_made_recipes');
    }
}
