<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersRateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_rate_recipes', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('recipe_id')->unsigned();
            $table->integer('rating')->unsigned();
            $table->softDeletes();
            $table->nullableTimestamps();

            $table->primary(['user_id', 'recipe_id']);

            $table->foreign('user_id', 'fk_users_rate_recipes_users_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('recipe_id', 'fk_users_rate_recipes_recipes_idx')
                ->references('id')->on('recipes')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_rate_recipes');
    }
}
