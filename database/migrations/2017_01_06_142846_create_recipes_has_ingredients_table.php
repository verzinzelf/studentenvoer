<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesHasIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes_has_ingredients', function (Blueprint $table) {
            $table->integer('recipe_id')->unsigned();
            $table->integer('ingredient_id')->unsigned();
            $table->integer('quantity')->unsigned();
            $table->string('unit');
            $table->softDeletes();
            $table->nullableTimestamps();

            $table->primary(['recipe_id', 'ingredient_id']);

            $table->foreign('recipe_id', 'fk_recipes_has_ingredients_recipes_idx')
                ->references('id')->on('recipes')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('ingredient_id', 'fk_recipes_has_ingredients_ingredients_idx')
                ->references('id')->on('ingredients')
                ->onDelete('cascade')
                ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes_has_ingredients');
    }
}
