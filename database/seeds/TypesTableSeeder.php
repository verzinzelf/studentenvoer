<?php

use Illuminate\Database\Seeder;
use App\Type;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = ['voorgerecht', 'hoofdgerecht', 'nagerecht', 'bijgerecht', 'snack'];

        foreach($type as $type){
            Type::create(array(
                'type' => $type,
            ));
        }
    }
}
