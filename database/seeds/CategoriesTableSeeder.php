<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = ['Italiaans', 'Nederlands', 'Grieks', 'Mediteraans', 'Spaans', 'Mexicaans', 'Duits', 'Turks'];

        asort($category); // sort the array asc

        foreach($category as $cat){
            Category::create(array(
                'category' => $cat,
            ));
        }
    }
}