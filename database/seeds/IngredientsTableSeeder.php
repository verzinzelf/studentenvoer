<?php

use Illuminate\Database\Seeder;
use App\Ingredient;

class IngredientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ingredient = ['suiker', 'melk', 'rijst', 'pasta', 'ui', 'zout', 'peper', 'peterselie', 'kaas', 'yoghurt', 'kip', 'rund',
                        'varken', 'lam', 'biefstuk', 'knoflook', 'wortel', 'appel', 'olijf', 'komkommer', 'bloem', 'eieren', 'prei', 'magic' ];

        asort($ingredient); // sort the array asc

        foreach($ingredient as $ingredient){
            Ingredient::create(array(
                'ingredient' => $ingredient,
            ));
        }
    }
}
