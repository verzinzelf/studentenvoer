<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(array(
            'username' => 'Verzinzelf',
            'firstname' => 'Pascal',
            'lastname' => 'van Egmond',
            'email' => 'pascalvanegmond@hotmail.com',
            'password' => bcrypt('Test123.'),
        ));
    }
}
