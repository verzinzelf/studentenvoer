@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Edit your recipe</h3> </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/recipe/'.$recipe->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-3 control-label">Name</label>

                                <div class="col-md-7">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') == null ? $recipe->name : old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="category" class="col-md-3 control-label">Category</label>
                                <div class="col-md-7">
                                    <select id="category" name="category" class="form-control">
                                        @foreach($category as $category)
                                            <option {{old('category') == $category->id || $recipe->category_id == $category->id ? 'selected' : '' }}
                                                    value='{{ $category->id }}'>{{ $category->category }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="type" class="col-md-3 control-label">Type</label>
                                <div class="col-md-7">
                                    <select id="type" name="type" class="form-control">
                                        @foreach($type as $type)
                                            <option {{old('type') == $type->id || $recipe->type_id == $type->id ? 'selected' : '' }}
                                                    value='{{ $type->id }}'>{{ $type->type }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('time') ? ' has-error' : '' }}">
                                <label for="time" class="col-md-3 control-label">Time</label>

                                <div class="col-md-7">
                                    <input id="time" type="text" class="form-control" name="time" value="{{ old('time') == null ? $recipe->time : old('time') }}">

                                    @if ($errors->has('time'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('calories') ? ' has-error' : '' }}">
                                <label for="calories" class="col-md-3 control-label">Calories</label>

                                <div class="col-md-7">
                                    <input id="calories" type="text" class="form-control" name="calories" value="{{ old('calories') == null ? $recipe->calories : old('calories') }}">

                                    @if ($errors->has('calories'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('calories') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group  {{ $errors->has('persons') ? ' has-error' : '' }}">
                                <label for="persons" class="col-md-3 control-label">Persons/ Servings</label>

                                <div class="col-md-7">
                                    <input id="persons" type="text" class="form-control" name="persons" value="{{ old('persons') == null ? $recipe->persons : old('persons') }}">

                                    @if ($errors->has('persons'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('persons') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group required {{  $errors->has('directions') ? ' has-error' : '' }}">
                                <label for="directions" class="col-md-3 control-label">Directions</label>

                                <div class="col-md-7">
                                    <textarea class="form-control" name='directions' rows="10" id="directions" required >{{ old('directions') == null ? $recipe->directions : old('directions') }}</textarea>

                                    @if ($errors->has('directions'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('directions') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            @foreach($recipe->ingredient as $key => $R_ingredient)
                                <div class="form-group">
                                    <label for="category" class="col-md-3 control-label">Ingredients</label>
                                    <div class="col-md-2">
                                        <input id="quantity" type="text" class="form-control" placeholder="quantity" name="quantity{{$key}}" value="{{ old('quantity' . $key) == null ? $R_ingredient->pivot->quantity : old('quantity'. $key) }}">
                                    </div>
                                    <div class="col-md-2">
                                        <input id="Unit" type="text" class="form-control" placeholder="unit (kg)" name="unit{{$key}}" value="{{ old('unit'. $key) == null ? $R_ingredient->pivot->unit : old('unit'. $key) }}">
                                    </div>
                                    <div class="col-md-3">
                                        <select id="ingredient" name="ingredient{{$key}}" class="form-control">
                                            @foreach($ingredients as $ingredient)
                                                <option {{old('ingredient'. $key) == $ingredient->id || $R_ingredient->pivot->ingredient_id == $ingredient->id ? 'selected' : '' }}
                                                        value='{{ $ingredient->id }}'>{{ $ingredient->ingredient }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <a href="#" class="btn btn-success">+</a>
                                </div>
                            @endforeach



                            <div class="form-group">
                                <div class="col-md-7 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                            <span class="pull-right red">* is required</span>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
