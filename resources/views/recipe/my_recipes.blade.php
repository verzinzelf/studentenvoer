@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>
            Recipes
            <a href="/recipe/create" class="btn btn-success">New</a>
        </h1>

        <div class="row">
            <h1>My Recipes</h1>
            @if($recipes->isEmpty())
                <h3>You currently dont have any recipes. Feel free to create <a href="/recipe/create">one</a>.</h3>
            @else
                <table class="table">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Time</th>
                        <th>category</th>
                        <th>type</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($recipes as $recipe)
                        <tr>
                            <td><a href="{{ url('/recipe/'.$recipe->id) }}">{{ $recipe->name }}</a></td>
                            <td>{{ $recipe->time }}</td>
                            <td>{{ $recipe->category->category }}</td>
                            <td>{{ $recipe->type->type }}</td>
                            <td>
                                <a href="/recipe/{{ $recipe->id }}/edit" class="btn btn-info">Edit</a>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete{{ $recipe->id }}">Delete</button>
                                <div class="modal fade" id="delete{{ $recipe->id }}" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Are you sure you want to delete this recipe?</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form id="delete" action="{{ url('/recipe/'.$recipe->id) }}" method="POST" >
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <button type="submit" class="btn btn-danger">Yes</button>
                                                    <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif

            @if(!$deleted_recipes->isEmpty())
                <h1>Recent deleted recipes</h1>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Time</th>
                        <th>category</th>
                        <th>type</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($deleted_recipes as $recipe)
                        <tr>
                            <td><a href="{{ url('/recipe/'.$recipe->id) }}">{{ $recipe->name }}</a></td>
                            <td>{{ $recipe->time }}</td>
                            <td>{{ $recipe->category->category }}</td>
                            <td>{{ $recipe->type->type }}</td>
                            <td>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#restore{{ $recipe->id }}">Restore</button>
                                <div class="modal fade" id="restore{{ $recipe->id }}" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Are you sure you want to restore this recipe?</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form id="restore" action="{{ url('/recipe/'.$recipe->id. '/restore') }}" method="POST" >
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger">Yes</button>
                                                    <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>

    </div>
@endsection
