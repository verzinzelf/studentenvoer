@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>
            Recipes
            <a href="/recipe/create" class="btn btn-success">New</a>
        </h1>

        <div class="row">
            <h1>Latest Recipes</h1>
            <table class="table">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Time</th>
                    <th>category</th>
                    <th>type</th>
                    <th>ingredient</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($recipes as $recipe)
                    <tr>
                        <td><a href="{{ url('/recipe/'.$recipe->id) }}">{{ $recipe->name }}</a></td>
                        <td>{{ $recipe->time }}</td>
                        <td>{{ $recipe->category->category }}</td>
                        <td>{{ $recipe->type->type }}</td>
                        <td>
                        @foreach($recipe->ingredient as $ingredient)
                            {{ $ingredient->pivot->quantity . $ingredient->pivot->unit . ' ' . $ingredient->ingredient }}
                        @endforeach
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
@endsection
