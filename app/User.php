<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $table = 'users';

    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at' ,
    ];

    protected $fillable = [
        'username', 'firstname', 'lastname', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function comment()
    {
        return $this->hasMany('App\Comment');
    }

    public function recipe()
    {
        return $this->hasMany('App\Recipe');
    }

    public function userMadeRecipe()
    {
        return $this->belongsToMany('App\Recipe', 'users_made_recipes')
            ->withTimestamps();
    }

    public function userRateRecipe()
    {
        return $this->belongsToMany('App\Recipe', 'users_rate_recipes')
            ->withPivot('rating')
            ->withTimestamps();
    }
}
