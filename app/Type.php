<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{
    use SoftDeletes;

    protected $table = 'types';

    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at' ,
    ];

    protected $fillable = [
        'type',
    ];


    public function recipe()
    {
        return $this->hasMany('App\Recipe');
    }

}
