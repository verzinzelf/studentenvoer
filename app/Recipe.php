<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recipe extends Model
{
    use SoftDeletes;

    protected $table = 'recipes';

    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at' ,
    ];

    protected $fillable = [
        'name', 'time', 'calories', 'persons', 'price', 'category_id', 'user_id', 'type_id', 'directions',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function comment()
    {
        return $this->hasMany('App\Comment');
    }

    public function ingredient()
    {
        return $this->belongsToMany('App\Ingredient', 'recipes_has_ingredients')
            ->withPivot('quantity', 'unit')
            ->withTimestamps();
    }

    public function userMadeRecipe()
    {
        return $this->belongsToMany('App\User', 'users_made_recipes')
            ->withTimestamps();
    }

    public function userRateRecipe()
    {
        return $this->belongsToMany('App\User', 'users_rate_recipes')
            ->withPivot('rating')
            ->withTimestamps();
    }

}
