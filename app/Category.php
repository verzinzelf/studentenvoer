<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $table = 'categories';

    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at' ,
    ];

    protected $fillable = [
        'category',
    ];

    public function recipe()
    {
        return $this->hasMany('App\Recipe');
    }
}
