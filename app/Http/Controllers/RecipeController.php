<?php

namespace App\Http\Controllers;
use App\Http\Requests\RecipeRequest;
use App\Ingredient;
use App\Recipe;
use App\Category;
use App\Type;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class RecipeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $recipes = Recipe::latest()->get();

        return view('recipe.index', compact('recipes'));
    }

    public function show(Recipe $recipe)
    {
        $category = Category::all();
        $type = Type::all();
        $ingredients = Ingredient::all();

        return view('recipe.show', compact('recipe', 'category', 'type', 'ingredients' ));
    }

    public function myRecipes()
    {
        $recipes = Recipe::where('user_id', Auth::id())->orderby('updated_at', 'desc')->get();
        $deleted_recipes = Recipe::onlyTrashed()->get();
        return view('recipe.my_recipes', compact('recipes', 'deleted_recipes'));
    }

    public function create()
    {
        $category = Category::all();
        $type = Type::all();
        $ingredients = Ingredient::all();

        return view('recipe.create', compact('category', 'type', 'ingredients'));
    }

    public function store(RecipeRequest $request)
    {

        $recipe = new Recipe([
            'name' => $request->name,
            'category_id' => (int)$request->category,
            'type_id' => (int)$request->type,
            'user_id' => Auth::user()->id,
            'time' => (int)$request->time,
            'calories' => (int)$request->calories,
            'persons' => (int)$request->persons,
            'directions' => $request->directions,
        ]);

        $recipe->save();


        $recipe->ingredient()->attach(Ingredient::find($request->ingredient),['quantity' => (int)$request->quantity, 'unit' => $request->unit]);

        return redirect('/recipe');
    }

    public function edit(Recipe $recipe)
    {
        $category = Category::all();
        $type = Type::all();
        $ingredients = Ingredient::all();

        return view('recipe.edit', compact('recipe','category', 'type', 'ingredients'));
    }

    public function update(RecipeRequest $request, Recipe $recipe)
    {

        $recipe->update([
            'name' => $request->name,
            'category_id' => (int)$request->category,
            'type_id' => (int)$request->type,
            'time' => (int)$request->time,
            'calories' => (int)$request->calories,
            'persons' => (int)$request->persons,
            'directions' => $request->directions,
        ]);

        $ingredient = [$request->ingredient0, ['quantity' => (int)$request->quantity0, 'unit' => $request->unit0]];

        $recipe->ingredient()->sync($ingredient);

        return redirect('/recipe');
    }

    public function destroy(Recipe $recipe)
    {
        $recipe->delete();

        return back();
    }

    public function restore($id) {

        Recipe::onlyTrashed()->where('id', $id)->restore();
        $recipe = Recipe::find($id);
        \Session::flash('success', 'Successfully restored the recipe: ' .  $recipe->name);

        return back();
    }
}
