<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecipeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:4',
            'category' => 'numeric',
            'type' => 'numeric',
            'time' => 'numeric',
            'calories' => 'numeric',
            'directions' => 'string|min:10|required',
//            'quantity' => 'numeric',
//            'ingredient' => 'numeric',
        ];
    }

//    public function all()
//    {
//        // get everything from request
//        $data = parent::all();
//
//        dd($data);
//    }
}
