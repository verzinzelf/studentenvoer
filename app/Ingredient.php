<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ingredient extends Model
{
    use SoftDeletes;

    protected $table = 'ingredients';

    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at' ,
    ];

    protected $fillable = [
        'ingredient',
    ];

    public function recipe()
    {
        return $this->belongsToMany('App\Recipe', 'recipes_has_ingredients')
            ->withPivot('quantity', 'unit')
            ->withTimestamps();
    }
}
