<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Auth::routes();

Route::get('/', 'RecipeController@index');


//Route::get('/recipe', 'RecipeController@index');

Route::resource('/recipe', 'RecipeController');
Route::get('/recipes', 'RecipeController@index');
Route::post('/recipe/{id}/restore', 'RecipeController@restore');
Route::get('/myrecipes', 'RecipeController@myRecipes');
